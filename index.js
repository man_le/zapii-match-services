var express = require('express');
var _ = require('lodash');
var cron = require('node-cron');
var app = express();
var md5 = require('md5');

// Email config
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
  service: 'gmail',
  port: 25,
  auth: {
    user: 'zapii.me.contact@gmail.com',
    pass: 'MinhMan89'
  }, tls: {
    rejectUnauthorized: false
  }
});


// Firestore function
const update = async (dbName, condition, data, isUpsert) => {
  if (_.isEmpty(condition) || condition.length < 3) {
    console.error('Cannot delete without condition!');
    return false;
  }
  try {
    var query = db.collection(dbName);
    query = query.where(condition[0], condition[1], condition[2]);

    const docs = await query.get();
    if (docs && !_.isEmpty(docs.docs)) {
      docs.docs.map(e => {
        db.collection(dbName).doc(e.id).update({ ...data, ...{ updatedDate: _.now() } });
      })
    }
    return true;
  } catch (err) {
    console.error(err);
  }
  return false;
}



// Init Firebase
var admin = require("firebase-admin");
var serviceAccount = require("./zapii-dff3b-firebase-adminsdk-73n3r-e1799f6495.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://zapii-dff3b.firebaseio.com"
});
let db = admin.firestore();

let dbRealTime = admin.database();
var ref = dbRealTime.ref("rt_user_online/user_online");

///////START - Remove Notification when no one online /////
ref.on("value", function (snapshot) {
  console.log('Start Removing Task For Notification at:' + new Date());
  const value = snapshot.val();
  if (_.isEmpty(value)) {

    // Remove all notification data 
    const rt_notification = dbRealTime.ref("rt_notification");
    const rt_notification_info = dbRealTime.ref("rt_notification_info");

    rt_notification.remove();
    rt_notification_info.remove();
  }
  console.log('End Removing Task For Notification at:' + new Date());
}, function (errorObject) {
  console.log("The read failed: " + errorObject.code);
});
////// END - Remove Notification when no one online //////////////////////////////////////////////////////

//// START - Send email include code to active account
var emailActiveRef = dbRealTime.ref("user_need_active");
emailActiveRef.on("value", async function (snapshot) {
  console.log('Start Send Active Code Task For Notification at:' + new Date());
  const keyValuePair = snapshot.val();

  if(_.isEmpty(keyValuePair)) return;

  const keys = Object.keys(keyValuePair);
  for (let i = 0; i < keys.length; i++) {
    const value = keyValuePair[keys[i]];
    const email = value[keys[i]];
    console.log(email);

    const randomNumber = Math.floor(100000 + Math.random() * 900000);
    var mailOptions = {
      from: 'zapii_no_reply@zapii.me',
      to: email,

      subject: '[ZAPII] mã kích hoạt',
      html: `Chào bạn, đây là mã kích hoạt để đăng nhập vào <a href='https://zapii.me'>zapii.me</a> : ${String(randomNumber)}`
    };

    // Update vcode inside user
    await update('user', ['_id', '==', keys[i]], { 'vcode': md5(String(randomNumber)) }, false);

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response + ' with active code: ' + String(randomNumber));
      }
    });

    setTimeout(()=>{
      const emailActiveRef = dbRealTime.ref("user_need_active/" + keys[i]);
      emailActiveRef.remove();
    },1000)
   
  }


  console.log('End Send Active Code Task For Notification at:' + new Date());
});

//// END - Send email include code to active account


// Run every 20 mins
var minutes = 20, the_interval = minutes * 60 * 1000;
setInterval(function () {

  // START - SEND EMAIL NOTIFY NEW ORDER

  // Get order data
  const orderRef = db.collection('order');
  orderRef.where('status', '==', 'pending').where('isCancel', '==', false).orderBy('createdDate').limit(100).get().then(data => {
    if (data.empty) {
      return;
    }

    let totalOrder = 0;

    let messageContent = 'Các đơn hàng đang chờ bạn xử lý: <br/>';
    data.forEach(doc => {
      totalOrder++;
      const itemData = doc.data();
      messageContent += `<strong>${itemData.order_id}</strong> ${itemData.address_value ? ' - ' + itemData.address_value : ''} - Tổng cộng: ${itemData.totalPrice} <br/>`
    });

    var mailOptions = {
      from: 'Zapii_no_reply@zapii.me',
      to: 'manle1389@gmail.com, minhduybk09@gmail.com, nhiepanhminh@gmail.com',

      subject: '[ZAPII] có ' + totalOrder + ' đơn hàng đang chờ bạn xử lý',
      html: messageContent
    };

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
      }
    });
  })
    .catch(err => {
      console.log('Error getting order data', err);
    });
  // END - SEND EMAIL NOTIFY NEW ORDER



}, the_interval);


const PORT = 3001;
app.listen(PORT, function () {
  console.log(`Zapii service service running on port ${PORT} ! `);
});